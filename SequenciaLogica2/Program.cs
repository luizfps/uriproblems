﻿using System;

namespace SequenciaLogica2
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split();
            var x = int.Parse(inputs[0]);
            var y = int.Parse(inputs[1]);

            for (int i = 1; i <= y; i++)
            {
                if (i % x == 0)
                    Console.WriteLine(i);
                else
                    Console.Write(i+" ");
            }
        }
    }
}