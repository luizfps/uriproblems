﻿using System;
using System.Linq;

namespace SortSimples
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Console.ReadLine().Split().Select(x => Convert.ToInt32(x)).ToArray();

            var numerosOrdenados= numeros.OrderBy(x => x).ToArray();

            foreach (var nm in numerosOrdenados)
                Console.WriteLine(nm);
            
            Console.WriteLine();
            
            foreach (var nm in numeros)
                Console.WriteLine(nm);
        }
    }
}