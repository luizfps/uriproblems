﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TempoDeJogoComMinutos
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => Convert.ToInt32(x)).ToArray();
            var horasEMinutosList = new List<int>();
            
            for (int i = 0; i < 1440; i++)
                horasEMinutosList.Add(i);

            horasEMinutosList = horasEMinutosList.Concat(horasEMinutosList).ToList();

            var horaMinutoInicial = inputs[0] * 60 + inputs[1];
            var horaMinutoFinal = inputs[2] * 60 + inputs[3];

            var indexHoraMinutoInicial= horasEMinutosList.FindIndex( (i => i == horaMinutoInicial));
            var indexHoraMinutoFinal = horasEMinutosList.FindIndex(horaMinutoInicial+1, (i => i == horaMinutoFinal));
            
            Console.WriteLine($"O JOGO DUROU {(indexHoraMinutoFinal-indexHoraMinutoInicial)/60} HORA(S) E {(indexHoraMinutoFinal-indexHoraMinutoInicial)%60} MINUTO(S)");

        }
    }
}