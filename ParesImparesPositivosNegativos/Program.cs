﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParesImparesPositivosNegativos
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = new List<int>();
            for (int i = 0; i < 5; i++)
                numeros.Add(Convert.ToInt32(Console.ReadLine()));
            var pares = numeros.Count(x => x % 2 == 0);
            var impares = numeros.Count(x => x % 2 != 0);
            var positivos = numeros.Count(x => x > 0);
            var negativos = numeros.Count(x => x < 0);
            Console.WriteLine("{0} valor(es) par(es)",pares);
            Console.WriteLine("{0} valor(es) impar(es)",impares);
            Console.WriteLine("{0} valor(es) positivo(s)",positivos);
            Console.WriteLine("{0} valor(es) negativo(s)",negativos);
        }
    }
}