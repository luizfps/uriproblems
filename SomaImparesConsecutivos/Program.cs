﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SomaImparesConsecutivos
{
    class Program
    {
        static void Main(string[] args)
        {
            int quantidadeCasosTeste = int.Parse( Console.ReadLine());
            var listasomaConsecutivos = new List<int>();

            for (int i = 0; i < quantidadeCasosTeste; i++)
            {
                var inputs = Console.ReadLine().Split().Select(x => Convert.ToInt32(x)).ToArray();

                listasomaConsecutivos.Add(Enumerable.Range(inputs[0], inputs[1]*2).Where(x => x % 2 != 0).Take(inputs[1]).Aggregate((elemento1, elemento2) => elemento1 + elemento2));
            }
            listasomaConsecutivos.ForEach(x => Console.WriteLine(x));
        }
    }
}
