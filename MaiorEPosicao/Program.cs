﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaiorEPosicao
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = new List<int>();
            for (int i = 0; i < 100; i++)
                numeros.Add(int.Parse(Console.ReadLine()));

            var maiorNumero = numeros.Max();
            var posicao = numeros.IndexOf(maiorNumero)+1;
            Console.WriteLine("{0}\n{1}",maiorNumero,posicao);
        }
    }
}