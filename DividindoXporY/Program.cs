﻿using System;

namespace DividindoXporY
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                var inputs = Console.ReadLine().Split();
                var num = double.Parse(inputs[0]);
                var ndois = double.Parse(inputs[1]);
                if(ndois == 0.0D)
                    Console.WriteLine("divisao impossivel");
                else
                    Console.WriteLine("{0:0.0}",num/ndois);
            }
        }
    }
}