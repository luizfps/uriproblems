﻿using System;
using System.Linq;

namespace SomandoInteirosConsecutivos
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => int.Parse(x)).Where(x => x > 0).ToArray();
            var soma = 0;
            
            for (int i = 0; i <= inputs[1]-1; i++)
            {
                soma += inputs[0]+ i;
            }
            Console.WriteLine(soma);
        }
    }
}