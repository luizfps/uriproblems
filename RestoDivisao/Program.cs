﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RestoDivisao
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = new List<int>
            {
                int.Parse(Console.ReadLine()),
                int.Parse(Console.ReadLine())
            };
            inputs.Sort();

            var soma = Enumerable.Range(inputs[0]+1, inputs[1] - inputs[0]-1).Where(x => x % 5 == 2 || x % 5 == 3);

            foreach (var i in soma)
            {
                Console.WriteLine(i);
            }
        }
    }
}
