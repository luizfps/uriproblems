﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Multiplos
{
    class Program
    {
        public static bool SaoMultiplos(int a,int b)
        {
            if (a > b)
                return a % b == 0;

            return b % a == 0;
        }

        public static Dictionary<bool, string> verificadorMultiplos = new Dictionary<bool, string>
        {
            {true, "Sao Multiplos"},
            {false, "Nao sao Multiplos"}
        };
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => Convert.ToInt32(x)).ToArray();
            Console.WriteLine(verificadorMultiplos[SaoMultiplos(inputs[0],inputs[1])]);
        }
    }
}