﻿using System;
using System.Collections.Generic;

namespace mes
{
    class Program
    {
        static void Main(string[] args)
        {
            var mes = Convert.ToInt32(Console.ReadLine());
            var meses = new Dictionary<int,string>
            {
                {1,"January"},
                {2,"February"},
                {3,"March"},
                {4,"April"},
                {5,"May"},
                {6,"June"},
                {7,"July"},
                {8,"August"},
                {9,"September"},
                {10,"October"},
                {11,"November"},
                {12,"December"},
            };
            Console.WriteLine(meses[mes]);
        }
    }
}