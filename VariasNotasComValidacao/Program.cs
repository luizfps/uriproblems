﻿using System;
using System.Globalization;

namespace VariasNotasComValidacao
{
    class Program
    {
        static void Main(string[] args)
        {
            var notaUm = Convert.ToDouble(Console.ReadLine(), new CultureInfo("en-Us"));
            var notasValidas = 0;
            var somaNotasValidas = 0.0;
            var repeticaoMenu = true;
            while (repeticaoMenu)
            {
                while (notasValidas < 2)
                {
                    if ((notaUm < 0.0 || notaUm > 10.0))
                        Console.WriteLine("nota invalida");
                    else
                    {
                        somaNotasValidas += notaUm;
                        notasValidas++;

                        if (notasValidas == 2)
                            continue;
                    }

                    notaUm = Convert.ToDouble(Console.ReadLine(), new CultureInfo("en-Us"));
                }

                Console.WriteLine("media = {0:0.00}", (somaNotasValidas) / 2);
                Console.WriteLine("novo calculo (1-sim 2-nao)");
                var novoCalculo = Convert.ToInt32(Console.ReadLine());
                while (novoCalculo != 2 && novoCalculo != 1)
                {
                    Console.WriteLine("novo calculo (1-sim 2-nao)");
                    novoCalculo = Convert.ToInt32(Console.ReadLine());
                }

                if (novoCalculo != 2)
                {
                    notaUm = Convert.ToDouble(Console.ReadLine(), new CultureInfo("en-Us"));
                }
                somaNotasValidas = 0;
                notasValidas = 0;
                repeticaoMenu = novoCalculo == 1;
            }
        }
    }
}