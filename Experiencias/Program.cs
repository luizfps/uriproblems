﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Experiencias
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var coelhos = 0;
            var sapos = 0;
            var ratos = 0;
            
            var calculoDic = new Dictionary<string,Action<int>>
            {
                {"R",(i => ratos+=i )},
                {"C",(i => coelhos+=i )},
                {"S",(i => sapos+=i )},
            };

            for (var i = 0; i < n; i++)
            {
                var entrada = Console.ReadLine().Split();

                var qtd = int.Parse(entrada[0], new CultureInfo("en-Us"));
                var tipoAnimal = entrada[1];
                calculoDic[tipoAnimal](qtd);
            }


            var totalAnimais = coelhos + ratos + sapos;
            
        
            Console.WriteLine(
                "Total: {0} cobaias\nTotal de coelhos: {1}\nTotal de ratos: {2}\nTotal de sapos: {3}\nPercentual de coelhos: {4:F2} %\nPercentual de ratos: {5:F2} %\nPercentual de sapos: {6:F2} %",
                totalAnimais, coelhos, ratos, sapos, (coelhos * 100F) / totalAnimais, (ratos * 100F) / totalAnimais, (sapos * 100F) / totalAnimais);
        }
    }
}