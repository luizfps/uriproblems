﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Gerais
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //SomaParesConsecutivos();
            //CrescimentoPopulacional();
            //NumeroPerfeito();
            //NumeroPrimo();
            //SubstituicaoEmVetor1();
            //PreenchimentoDeVetor();
            //SelecaoEmVetor();
            //TrocaEmVetor();
            //FibonacciRecursivo();
            //PreenchimentoVetor2();
            //PreenchimentoVetor3();
            //PreenchimentoVetor4();
            //MenorPosicao();
            //LinhaMatriz();
            //ColunaMatriz();
            //AcimaDiagonalPrincipal();
            //AbaixoDiagonalPrincipal();
            //AcimaDiagonalSecundaria();
            // AbaixoDiagonalSecundaria();
            AreaSuperior();

        }

        private static void AreaSuperior()
        {
            var operacoes = new Dictionary<string, Func<double, int, double>>{
                {"S",(value,quatidade)=> value },
                {"M",(value,quantidade)=> value/quantidade}
            };
            var matriz = new double[12, 12];
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            var qtd = 0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j>i && j<11-i)
                    {
                        soma += numero;
                        qtd++;
                    }

                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma, qtd).ToString("F1").Replace(',', '.'));
        }

        private static void AbaixoDiagonalSecundaria()
        {
            var operacoes = new Dictionary<string, Func<double, int, double>>{
                {"S",(value,quatidade)=> value },
                {"M",(value,quantidade)=> value/quantidade}
            };
            var matriz = new double[12, 12];
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            var qtd = 0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j > 11 - i)
                    {
                        soma += numero;
                        qtd++;
                    }

                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma, qtd).ToString("F1").Replace(',', '.'));

        }

        private static void AcimaDiagonalSecundaria()
        {
            var operacoes = new Dictionary<string, Func<double, int, double>>{
                {"S",(value,quatidade)=> value },
                {"M",(value,quantidade)=> value/quantidade}
            };
            var matriz = new double[12, 12];
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            var qtd = 0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j<11-i)
                    {
                        soma += numero;
                        qtd++;
                    }

                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma, qtd).ToString("F1").Replace(',', '.'));
        }

        private static void AbaixoDiagonalPrincipal()
        {
            var operacoes = new Dictionary<string, Func<double, int, double>>{
                {"S",(value,quatidade)=> value },
                {"M",(value,quantidade)=> value/quantidade}
            };
            var matriz = new double[12, 12];
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            var qtd = 0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j < i)
                    {
                        soma += numero;
                        qtd++;
                    }

                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma, qtd).ToString("F1").Replace(',', '.'));
        }

        private static void AcimaDiagonalPrincipal()
        {
            var operacoes = new Dictionary<string, Func<double,int, double>>{
                {"S",(value,quatidade)=> value },
                {"M",(value,quantidade)=> value/quantidade}
            };
            var matriz = new double[12, 12];
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            var qtd = 0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j > i)
                    {
                        soma += numero;
                        qtd++;
                    }
                       
                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma,qtd).ToString("F1").Replace(',', '.'));
        }

        private static void ColunaMatriz()
        {

            var operacoes = new Dictionary<string, Func<double, double>>{
                {"S",(value)=> value },
                {"M",(value)=> value/12.0 }
            };
            var matriz = new double[12, 12];

            var coluna = Convert.ToInt32(Console.ReadLine());
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (j == coluna)
                        soma += numero;
                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma).ToString("F1").Replace(',', '.'));


        }

        private static void LinhaMatriz()
        {
            var operacoes = new Dictionary<string, Func<double, double>>{
                {"S",(value)=> value },
                {"M",(value)=> value/12.0 }
            };
            var matriz = new double[12, 12];

            var linha = Convert.ToInt32(Console.ReadLine());
            var operacao = (Console.ReadLine());
            var soma = 0.0;
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    var numero = Convert.ToDouble(Console.ReadLine());
                    if (i == linha)
                        soma += numero;
                }
            }

            Console.WriteLine(operacoes[operacao].Invoke(soma).ToString("F1").Replace(',', '.'));
        }

        private static void MenorPosicao()
        {
            var n = Convert.ToInt32(Console.ReadLine());
            var numero = Console.ReadLine().Split().Where(x=>{
                var sucesso = Int32.TryParse(x, out var resultado);
                return sucesso;
                
            }).Select(x => Convert.ToInt32(x)).ToArray();
            var menorNumero = numero.Min();
            var posicao = 0 ;
            for (int i = 0; i < numero.Length; i++)
            {
                if (numero[i] == menorNumero)
                    posicao = i;
            }
            Console.WriteLine("Menor valor: {0}", menorNumero);
            Console.WriteLine("Posicao: {0}", posicao);
        }

        private static void PreenchimentoVetor4()
        {
            var par = new int?[5]{null,null,null,null,null};
            var impar = new int?[5]{ null, null, null, null, null };

            for (int i = 0; i < 15; i++)
            {
                var numero = Convert.ToInt32(Console.ReadLine());

                if (numero % 2 == 0)
                {
                    if (par[4] == null)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            if (par[j] == null)
                            {
                                par[j] = numero;
                                break;
                            }
                                
                        }
                    }
                    else
                    {
                        for (int k = 0; k < 5; k++)
                        {
                            Console.WriteLine("par[{0}] = {1}", k, par[k]);
                            par[k] = null;
                        }
                        par[0] = numero;
                    }
                }
                else
                {
                    if (impar[4] == null)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            if (impar[j] == null)
                            {
                                impar[j] = numero;
                                break;
                            }
                                
                        }
                    }
                    else
                    {
                        for (int k = 0; k < 5; k++)
                        {
                            Console.WriteLine("impar[{0}] = {1}", k, impar[k]);
                            impar[k] = null;
                        }
                        impar[0] = numero;
                    }
                }
            }
            for (int i = 0; i < 5; i++)
            {
                if(impar[i]!=null)
                Console.WriteLine("impar[{0}] = {1}", i, impar[i]);
            }
            for (int i = 0; i < 5; i++)
            {
                if (par[i] != null)
                    Console.WriteLine("par[{0}] = {1}", i, par[i]);
            }
        }

        private static void PreenchimentoVetor3()
        {
            var valor = Convert.ToDecimal(Console.ReadLine(), new CultureInfo("en-Us"));
            var vetor = new decimal[100];
            vetor[0] = valor;
            Console.WriteLine("N[0] = {0}", Math.Round(valor,4).ToString().Replace(',', '.'));
            for (int i = 1; i < vetor.Length; i++)  
            {
                vetor[i] = vetor[i - 1] / 2;
                Console.WriteLine("N[{0}] = {1}", i,(Math.Round(vetor[i], 4).ToString().Replace(',','.')));
            }

        }

        private static void PreenchimentoVetor2()
        {
            var t = LerInteiro();
            var vetor = new int[1000];
            var indexComplementar = 0;
            for (int i = 0; i < vetor.Length; i++)
            {

                if (indexComplementar == t)
                {
                    indexComplementar = 0;
                    vetor[i] = indexComplementar;
                    indexComplementar++;
                }
                else
                {
                    vetor[i] = indexComplementar;
                    indexComplementar++;
                }
                Console.WriteLine("N[{0}] = {1}", i, vetor[i]);
            }
        }

        private static int LerInteiro() => Convert.ToInt32(Console.ReadLine());

        private static void FibonacciRecursivo()
        {
            var casosTeste = Convert.ToInt32(Console.ReadLine());
            var fib = new long[61];
            fib[0] = 0;
            fib[1] = 1;
            for (int i = 0; i < casosTeste; i++)
            {
                var n = Convert.ToInt32(Console.ReadLine());

                for (int j = 0; j <= n; j++)
                {
                    if (j > 1)
                    {
                        fib[j] = fib[j - 1] + fib[j - 2];
                    }
                }
                Console.WriteLine("fib({0}) = {1}", n, fib[n]);
            }
        }

        private static void TrocaEmVetor()
        {
            var vetor = new int[20];

            for (int i = 0; i < 20; i++)
            {
                vetor[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine("N[{0}] = {1}", i, vetor[19 - i]);
            }
        }

        private static void SelecaoEmVetor()
        {
            var vetor = new double[100];

            for (int i = 0; i < 100; i++)
            {
                vetor[i] = Convert.ToDouble(Console.ReadLine(), new CultureInfo("en-Us"));
                if (vetor[i] <= 10)
                    Console.WriteLine("A[{0}] = {1}", i, vetor[i].ToString("F1").Replace(',', '.'));
            }
        }

        public static void SomaParesConsecutivos()
        {
            int x;
            do
            {
                x = Convert.ToInt32(Console.ReadLine());

                if (x != 0)
                    Console.WriteLine(Enumerable.Range(x, 10).Where(y => y % 2 == 0).Take(5).Aggregate((num1, num2) => num1 + num2));

            } while (x != 0);
        }
        public static void CrescimentoPopulacional()
        {
            var casosTeste = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < casosTeste; i++)
            {
                var inputs = Console.ReadLine().Split();
                var PA = Convert.ToInt32(inputs[0]);
                var PB = Convert.ToInt32(inputs[1]);
                var G1 = Convert.ToDouble(inputs[2], new CultureInfo("en-Us"));
                var G2 = Convert.ToDouble(inputs[3], new CultureInfo("en-Us"));
                var anos = 0;
                while (anos <= 100 && PA <= PB)
                {
                    PA = (int)(PA + (PA * G1 / 100));
                    PB = (int)(PB + (PB * G2 / 100));
                    anos++;
                }
                if (anos <= 100)
                    Console.WriteLine(anos + " anos.");
                else
                    Console.WriteLine("Mais de 1 seculo.");
            }
        }
        public static void NumeroPerfeito()
        {
            var testes = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < testes; i++)
            {
                var x = Convert.ToInt32(Console.ReadLine());
                var numeroPerfeito = Enumerable.Range(1, x).Where(y => y != x && x % y == 0);
                    
                var nperfeito = numeroPerfeito.Count()>1 && numeroPerfeito.Aggregate((num1, num2) => num1 + num2) == x;
                if (nperfeito)
                    Console.WriteLine(x + " eh perfeito");
                else
                    Console.WriteLine(x + " nao eh perfeito");
            }
        }

        public static void NumeroPrimo()
        {
            var testes = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < testes; i++)
            {
                var x = Convert.ToInt32(Console.ReadLine());
                var numeros = Enumerable.Range(1, x).Where(y =>x % y == 0);

                var nperfeito = numeros.Count() == 2;
                if (nperfeito)
                    Console.WriteLine(x + " eh primo");
                else
                    Console.WriteLine(x + " nao eh primo");
            }
        }
        public static void SubstituicaoEmVetor1()
        {
            var vetor = new int[10];
            for (int i = 0; i < 10; i++)
            {
                var x = Convert.ToInt32(Console.ReadLine());
                vetor[i] = x <= 0 ? 1 : x;
            }
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"X[{i}] = {vetor[i]}");
            }
        }
        public static void PreenchimentoDeVetor()
        {
            var vetor = new int[10];
            vetor[0] = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < 10; i++)
            {
                vetor[i] = vetor[i - 1] * 2;
            }
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"N[{i}] = {vetor[i]}");
            }
        }
    }
}
