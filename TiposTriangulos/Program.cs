﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace TiposTriangulos
{
    class Program
    {
        public static bool NaoEhTriangulo(decimal A, decimal B, decimal C) => A >= B + C;
        public static bool TrianguloRetangulo(decimal A, decimal B, decimal C) => (A * A == ((B * B) + (C * C)));
        public static bool TrianguloObtusangulo(decimal A, decimal B, decimal C) => ((A * A) > ((B * B) + (C * C)));
        public static bool TrianguloAcutangulo(decimal A, decimal B, decimal C) => (A * A < ((B * B) + (C * C)));
        public static bool TrianguloEquilatero(decimal A, decimal B, decimal C) => (A == B && B == C);
        public static bool TrianguloIsosceles(decimal A, decimal B, decimal C) => (A == B && C != A) || (A == C && B != A)||(B==C && C!=A);
        
        public  static List<Tuple<Func<decimal,decimal,decimal,bool>,string>> TiposTriangulos = new List<Tuple<Func<decimal, decimal, decimal, bool>, string>>
        {
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(NaoEhTriangulo,"NAO FORMA TRIANGULO"),
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(TrianguloRetangulo,"TRIANGULO RETANGULO"),
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(TrianguloObtusangulo,"TRIANGULO OBTUSANGULO"),
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(TrianguloAcutangulo,"TRIANGULO ACUTANGULO"),
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(TrianguloEquilatero,"TRIANGULO EQUILATERO"),
            new Tuple<Func<decimal, decimal, decimal, bool>, string>(TrianguloIsosceles,"TRIANGULO ISOSCELES")
        };
        
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => Convert.ToDecimal(x,new CultureInfo("en-US") )).OrderByDescending(x=>x).ToArray();
            var triangulos = TiposTriangulos.Where(x => x.Item1(inputs[0], inputs[1], inputs[2])).Select(x => x.Item2)
                .ToArray();
            triangulos = triangulos.Length>1 && triangulos.Contains("NAO FORMA TRIANGULO")?new string[]{"NAO FORMA TRIANGULO"}:triangulos;
           
            foreach (var triangulo in triangulos)
               Console.WriteLine(triangulo); 
        }
    }
}