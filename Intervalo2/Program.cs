﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Intervalo2
{
    class Program
    {
        static void Main(string[] args)
        {
            var qtsInputs = int.Parse(Console.ReadLine());
            var numeros = new List<int>();
            for (int i = 0; i < qtsInputs; i++)
                numeros.Add(int.Parse(Console.ReadLine()));
            
            Console.WriteLine("{0} in",numeros.Count((x)=>x>=10&&x<=20));
            Console.WriteLine("{0} out",numeros.Count(x=>x<10 ||x>20));
        }
    }
}