﻿using System;
using System.Linq;

namespace TempoDeJogo
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => Convert.ToInt32(x)).ToArray();

            var horas = new[]
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}.ToList();

            var horaInicial = inputs[0];
            var horaFinal = inputs[1];
         
            var indexHoraInicial = horas.FindIndex((i => i == horaInicial ));
            var indexHoraFinal = horas.FindIndex(horaInicial+1, (i => i == horaFinal));
            
            Console.WriteLine($"O JOGO DUROU {indexHoraFinal-indexHoraInicial} HORA(S)");
        }
    }
}