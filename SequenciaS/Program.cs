﻿using System;

namespace SequenciaS
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = 1.0 ;
            for (int i = 2; i <= 100; i++)
                s += 1.0 / i;
            Console.WriteLine("{0:0.00}",s);
        }
    }
}