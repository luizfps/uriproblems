﻿using System;
using System.Collections.Generic;

namespace TipoCombustivel
{
    class Program
    {
        static void Main(string[] args)
        {
            var alcool = 0;
            var gasolina = 0;
            var diesel = 0;
            var fim = false;

            var combustiveis = new Dictionary<int, Action>
            {
                {1, (() => alcool++)},
                {2, (() => gasolina++)},
                {3, (() => diesel++)},
                {4, (() => fim = true)}
            };

            while (!fim)
            {
                try
                {
                     combustiveis[int.Parse(Console.ReadLine())]();
                }
                catch (Exception e)
                {
                }
            }
            Console.WriteLine("MUITO OBRIGADO\nAlcool: {0}\nGasolina: {1}\nDiesel: {2}",alcool,gasolina,diesel);
        }
    }
}