﻿using System;

namespace Grenais
{
    class Program
    {
        static void Main(string[] args)
        {
            var vitoriasInter = 0;
            var vitoriasGremio = 0;
            var empates = 0;
            var grenais = 0;
            var repeteGrenal = true;
            
            
            while (repeteGrenal)
            {
                grenais++;

                var gols = Console.ReadLine().Split();
                var golsInter = int.Parse(gols[0]);
                var golsGremio = int.Parse(gols[1]);

                if (golsInter > golsGremio)
                    vitoriasInter++;
                else if (golsGremio > golsInter)
                    vitoriasGremio++;
                else
                    empates++;
                
                Console.WriteLine("Novo grenal (1-sim 2-nao)");
                var novoGrenal = int.Parse(Console.ReadLine());
                repeteGrenal = novoGrenal == 1;
            }
            Console.WriteLine("{0} grenais\nInter:{1}\nGremio:{2}\nEmpates:{3}\n{4}",grenais,vitoriasInter,vitoriasGremio,empates,vitoriasInter>vitoriasGremio?"Inter venceu mais":vitoriasGremio>vitoriasInter?"Gremio venceu mais":"Nao houve vencedor");
        }
    }
}