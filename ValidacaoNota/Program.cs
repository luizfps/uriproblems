﻿using System;
using System.Globalization;

namespace ValidacaoNota
{
    class Program
    {
        static void Main(string[] args)
        {
            var notaUm = Convert.ToDouble(Console.ReadLine(),new CultureInfo("en-Us"));
            var notasValidas = 0;
            var somaNotasValidas = 0.0;
                
            while (notasValidas<2)
            {
                if ((notaUm < 0.0 || notaUm > 10.0 ))
                    Console.WriteLine("nota invalida");
                else
                {
                    somaNotasValidas += notaUm;
                    notasValidas++;
                    
                    if(notasValidas==2)
                        continue;
                }
                notaUm = Convert.ToDouble(Console.ReadLine(),new CultureInfo("en-Us"));
            }
            Console.WriteLine("media = {0:0.00}",(somaNotasValidas)/2);
        }
    }
}