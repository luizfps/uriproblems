﻿using System;
using System.Collections.Generic;

namespace ParOuImpar
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var numeros = new List<int>();
            for (int i = 1; i <= n; i++)
                numeros.Add(int.Parse(Console.ReadLine()));
            
            numeros.ForEach((x) =>
            {
                if (x > 0)
                    Console.WriteLine(x % 2 == 0 ? "EVEN POSITIVE" : "ODD POSITIVE");
                else if(x<0)
                    Console.WriteLine(x % 2 == 0 ? "EVEN NEGATIVE" : "ODD NEGATIVE");
                else
                    Console.WriteLine("NULL");
            });
        }
    }
}