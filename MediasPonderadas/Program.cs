﻿using System;
using System.Globalization;
using System.Linq;

namespace MediasPonderadas
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var medias = new Double[n, 3];
            
            for (int i = 0; i < n; i++)
            {
                 var entradas = Console.ReadLine().Split().Select(x => Convert.ToDouble(x,new CultureInfo("en-Us"))).ToArray();
                 medias[i, 0] = entradas[0]*2;
                 medias[i, 1] = entradas[1]*3;
                 medias[i, 2] = entradas[2]*5;
            }
            for (int i = 0; i < n; i++)
                Console.WriteLine(((medias[i,0]+medias[i,1]+medias[i,2])/10.00).ToString("F1").Replace(',','.'));
        }
    }
}