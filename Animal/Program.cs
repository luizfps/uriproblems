﻿using System;
using System.Collections.Generic;


namespace Animal
{
    class Program
    {
       static void Main(string[] args)
        {
            var dicionarioAnimais = new Dictionary<string, string>
            {
                {"vertebradoavecarnivoro", "aguia"},
                {"vertebradoaveonivoro", "pomba"},
                {"vertebradomamiferoonivoro", "homem"},
                {"vertebradomamiferoherbivoro", "vaca"},
                {"invertebradoinsetohematofago", "pulga"},
                {"invertebradoinsetoherbivoro", "lagarta"},
                {"invertebradoanelideohematofago", "sanguessuga"},
                {"invertebradoanelideoonivoro", "minhoca"},
            };
            var palavraUm = Console.ReadLine();
            var palavraDois = Console.ReadLine();
            var palavraTres = Console.ReadLine();
            var palavrasConcatenadas = palavraUm + palavraDois + palavraTres;
            Console.WriteLine(dicionarioAnimais[palavrasConcatenadas]);
        }
    }
}