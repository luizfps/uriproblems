﻿using System;
using System.Linq;

namespace Tabuada
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var tabuada = Enumerable.Range(1, 10).Select(x => x * n);
            var iterador = 1;
            foreach (var i in tabuada)
            {
                Console.WriteLine($"{iterador} x {n} = {i}");
                iterador++;
            }
        }
    }
}