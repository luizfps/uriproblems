﻿using System;

namespace SequenciaS2
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = 1.0 ;
            var denominador = 2.0;
            for (int i = 3; i <= 39; i += 2)
            {
                s += i/denominador;
                denominador *= 2;
            }
              
            Console.WriteLine("{0:0.00}",s);
        }
    }
}