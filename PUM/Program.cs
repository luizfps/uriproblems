﻿using System;

namespace PUM
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n*4; i++)
            {
                
                if(i%4==0)
                    Console.WriteLine("PUM");
                else
                    Console.Write(i+" ");
            }
        }
    }
}