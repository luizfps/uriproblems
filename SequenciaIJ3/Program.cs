﻿using System;

namespace SequenciaIJ3
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 7;
            var b = 6;
            var c = 5;

            for (int j = 1; j <= 9; j = j += 2)
            {
                Console.WriteLine($"I={j} J={a}\nI={j} J={b}\nI={j} J={c}");
                a += 2;
                b += 2;
                c += 2;
            }
        }
    }
}