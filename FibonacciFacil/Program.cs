﻿using System;

namespace FibonacciFacil
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var fibonacci = new int[n];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            for (int i = 2; i < n; i++)
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            for (int i = 0; i < n; i++)
            {
                if(i==n-1)
                    Console.WriteLine(fibonacci[i]);
                else
                {
                    Console.Write(fibonacci[i]+" ");
                }
            }
        }
    }
}