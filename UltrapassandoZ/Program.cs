﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace UltrapassandoZ
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = int.Parse(Console.ReadLine());
            var z = int.Parse(Console.ReadLine());
            while (z<=x)
                z = int.Parse(Console.ReadLine());

            var list = new List<int> {x};
            while (list.Sum()<=z)
                list.Add(list.Last()+1);
            
            Console.WriteLine(list.Count);
        }
    }
}