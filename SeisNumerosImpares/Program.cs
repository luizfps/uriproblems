﻿using System;

namespace SeisNumerosImpares
{
    class Program
    {
        static void Main(string[] args)
        {
            var numero = int.Parse(Console.ReadLine());
            var qtdNumerosImpares = 0;
            while (qtdNumerosImpares<6)
            {
                if (numero % 2 != 0)
                {
                    Console.WriteLine(numero);
                    numero = numero + 2;
                    qtdNumerosImpares++;
                }
                else
                {
                    numero = numero + 1;
                }
            }
        }
    }
}