﻿using System;

namespace SequenciaIJ2
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int j = 1; j <=9; j = j+=2)
                Console.WriteLine($"I={j} J=7\nI={j} J=6\nI={j} J=5");
        }
    }
}