﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Quadrante
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split();
            var xis = int.Parse(inputs[0]);
            var ypslon = int.Parse(inputs[1]);

            while (xis!=0&&ypslon!=0)
            {
                if(xis>0 && ypslon>0)
                    Console.WriteLine("primeiro");
                else if(xis<0 && ypslon>0)
                    Console.WriteLine("segundo");
                else if(xis<0 && ypslon<0)
                    Console.WriteLine("terceiro");
                else
                    Console.WriteLine("quarto");
                
                 inputs = Console.ReadLine().Split();
                 xis = int.Parse(inputs[0]);
                 ypslon = int.Parse(inputs[1]);
            }
        }
    }
}