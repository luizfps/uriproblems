﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CrescenteDecrescente
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split();
            var numeroUm = int.Parse(inputs[0]);
            var numeroDois = int.Parse(inputs[1]);

            while (numeroUm!=numeroDois)
            {
                Console.WriteLine(numeroUm < numeroDois ? "Crescente" : "Decrescente");

                inputs = Console.ReadLine().Split();
                numeroUm = int.Parse(inputs[0]);
                numeroDois = int.Parse(inputs[1]);
            }
        }
    }
}