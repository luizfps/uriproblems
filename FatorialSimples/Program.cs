﻿using System;

namespace FatorialSimples
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var multiplicacao = 1;
            for (int i = 1; i <=n; i++)
            {
                multiplicacao *= i;
            }
            Console.WriteLine(multiplicacao);
        }
    }
}