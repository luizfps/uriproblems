﻿using System;
using System.Linq;

namespace Resto2
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var restos = Enumerable.Range(1, 10000).Where(x => x % n == 2);
            foreach (var resto in restos)
                Console.WriteLine(resto);
        }

    }
}
