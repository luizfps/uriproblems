﻿using System;

namespace SequenciaCrescente
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = 1;
            while (x!=0)
            {
                int i = 1;
                x = int.Parse(Console.ReadLine());
                for ( i = 1; i < x; i++)
                        Console.Write(i+" "); 
                if(x!=0)Console.WriteLine(i);
            }
        }
    }
}