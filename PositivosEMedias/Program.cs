﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PositivosEMedias
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = new List<double>();
            for (int i = 0; i < 6; i++)
                numeros.Add(Convert.ToDouble(Console.ReadLine(),new CultureInfo("en-Us")));

            var quantidadeNumerosPositivos = numeros.Count(x => x > 0);
            var somaNumerosPares = (float)numeros.Where(x => x > 0).Aggregate(((d, d1) => d + d1));
            var media = (somaNumerosPares / quantidadeNumerosPositivos).ToString("F1").Replace(',','.');

            Console.WriteLine("{0} valores positivos\n{1}",quantidadeNumerosPositivos,media);
        }
    }
}