﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Triangulo
{
    class Program
    {
        public static bool EhTriangulo(decimal A, decimal B, decimal C) =>A+B>C && C+B>A && A+C>B;
        public static string Perimetro(decimal A,decimal B,decimal C)=>$"Perimetro = {String.Format("{0:0.0}", A+B+C)}".Replace(',','.');

        public static string Area(decimal A, decimal B, decimal C) => $"Area = {String.Format("{0:0.0}",((A+B)*C)/2)}".Replace(',','.');
        
        
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split().Select(x => Convert.ToDecimal(x,new CultureInfo("en-US"))).ToArray();
            
           var operacoesTriangulares = new Dictionary<bool,Func<decimal,decimal,decimal,string>>
           {
               {true,Perimetro},
               {false,Area}
           };
            
           Console.WriteLine(operacoesTriangulares[EhTriangulo(inputs[0],inputs[1],inputs[2])].Invoke(inputs[0],inputs[1],inputs[2]));
        }
    }
}