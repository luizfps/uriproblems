﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace AumentoSalario
{
    class Program
    {
        
        private static List<Tuple<Func<decimal,bool>,int>> _tabelaAumento = new List<Tuple<Func<decimal, bool>, int>>
        {
            new Tuple<Func<decimal, bool>, int>((x=>x>=0 && x<=(decimal) 400.00),15), 
            new Tuple<Func<decimal, bool>, int>((x=>x>=(decimal) 400.01 && x<=(decimal) 800.00),12),  
            new Tuple<Func<decimal, bool>, int>((x=>x>=(decimal) 800.01 && x<=(decimal) 1200.00),10),  
            new Tuple<Func<decimal, bool>, int>((x=>x>=(decimal) 1200.01 && x<=(decimal)2000.00),7),  
            new Tuple<Func<decimal, bool>, int>((x=>x>2000),4),  
        };
        static void Main(string[] args)
        {
            var salario = Console.ReadLine().Split().Select(x => Convert.ToDecimal(x,new CultureInfo("en-Us"))).First();
            var porcentagemAumento = _tabelaAumento.Single((f) => f.Item1(salario)).Item2;
            
            var reajuste = ((float)salario * (double)((float)porcentagemAumento/100));
            var novoSalario = (float)salario + reajuste;
            
            Console.WriteLine(string.Format("Novo salario: {0:0.00}\nReajuste ganho: {1:0.00}\nEm percentual: {2} %",novoSalario,reajuste,porcentagemAumento).Replace(",","."));
            
        }
    }
}