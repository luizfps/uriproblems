﻿using System;
using System.Linq;

namespace SomaImparesConsecutivosII
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var somaImpares = new int[n];
            for (var i = 0; i < n; i++)
            {
                var inputs = Console.ReadLine().Split();
                var numeroUm = int.Parse(inputs[0]);
                var numeroDois = int.Parse(inputs[1]);
                if (numeroUm<= numeroDois)
                {
                    while (++numeroUm<numeroDois)
                        somaImpares[i] += numeroUm % 2 != 0 ? numeroUm: 0;
                }
                else
                {
                    while (numeroUm>++numeroDois)
                        somaImpares[i] += numeroDois % 2 != 0 ? numeroDois : 0;
                }
            }
            for (var i = 0; i < n; i++)
                Console.WriteLine(somaImpares[i]);
        }
    }
}