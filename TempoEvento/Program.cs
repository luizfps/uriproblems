﻿    using System;
    using System.Linq;

namespace TempoEvento
{
    class Program
    {
        static void Main(string[] args)
        {
            var diaInicio = Convert.ToInt32(Console.ReadLine().Split()[1]);
            var horaIncio = Console.ReadLine().Split(':').Select(x=>Convert.ToInt32(x)).ToArray();
            
            var diaFim = Convert.ToInt32(Console.ReadLine().Split()[1]);
            var horaFim = Console.ReadLine().Split(':').Select(x=>Convert.ToInt32(x)).ToArray();

            var diferencaDias = ((diaFim - diaInicio)+1 )* 86400;
            var diferencaDiaInicial =  ((horaIncio[0] * 3600) + (horaIncio[1] * 60 )+ horaIncio[2]);
            var diferencaDiaFinal = 86400 - ((horaFim[0] * 3600) + (horaFim[1] * 60 )+ horaFim[2]);
            var tempoEmSegundosInicioFim = diferencaDias - diferencaDiaInicial - diferencaDiaFinal;
            var diasDuracao = tempoEmSegundosInicioFim / 86400;
            var horasDuracao = (tempoEmSegundosInicioFim % 86400) / 3600;
            var minutosDuracao = ((tempoEmSegundosInicioFim % 86400) % 3600)/60;
            var segundosDuracao = ((tempoEmSegundosInicioFim % 86400) % 3600) % 60;
            
            Console.WriteLine("{0} dia(s)\n{1} hora(s)\n{2} minuto(s)\n{3} segundo(s)",diasDuracao,horasDuracao,minutosDuracao,segundosDuracao);
        }
    }
}