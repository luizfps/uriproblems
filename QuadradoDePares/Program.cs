﻿using System;
using System.Linq;

namespace QuadradoDePares
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            foreach (var i in Enumerable.Range(1, n).Where(x => x % 2 == 0).ToArray())
            {
                Console.WriteLine($"{i}^2 = {i*i}");  
            }
        }
    }
}