﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NumerosPositivos
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = new List<double>();
            for (var i = 0; i < 6; i++)
                numeros.Add(Convert.ToDouble(Console.ReadLine()));
            
            Console.WriteLine($"{numeros.Count(x => x>0)} valores positivos");
        }
    }
}