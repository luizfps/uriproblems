﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ImpostoRenda
{
    class Program
    {
        static void Main(string[] args)
        {
            var percentualImposto = new[] {0, 8, 18, 28}.ToList();
            var limiteMaximoPorPercentual = new []{2000, 3000, 4500, long.MaxValue};
            var tabelaImpostoRenda = new List<Tuple<Func<double, bool>, int>>
            {
                new Tuple<Func<double, bool>, int>(((x) => x >= (double) 0.00 && x <= (double) 2000.00), percentualImposto[0]),
                new Tuple<Func<double, bool>, int>(((x) => x >= (double) 2000.01 && x <= (double) 3000.00), percentualImposto[1]),
                new Tuple<Func<double, bool>, int>(((x) => x >= (double) 3000.01 && x <= (double) 4500.00), percentualImposto[2]),
                new Tuple<Func<double, bool>, int>(((x) => x > (double)4500.00 ), percentualImposto[3])
            };

            var salario = Console.ReadLine().Split().Select(x => Convert.ToDouble(x,new CultureInfo("en-Us"))).First();

            var percentual =  tabelaImpostoRenda.Single(x => x.Item1(salario)).Item2;

            if(percentual==0)
                Console.WriteLine( "Isento");
            else
            {
                var imposto = 0.00;
                for (int i = percentualImposto.IndexOf(percentual); i > 0; i--)
                {
                    imposto = imposto + ((double)salario - limiteMaximoPorPercentual[i - 1]) * (percentualImposto[i]/100.00);
                    salario = salario - ( salario - limiteMaximoPorPercentual[i - 1]);
                }
                var respostaFormatada = $"R$ {imposto:0.00}".Replace(",",".");
            
                Console.WriteLine(respostaFormatada);
            }
        }
    }
}