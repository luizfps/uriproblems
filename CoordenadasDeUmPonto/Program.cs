﻿using System;
using System.Collections.Generic;
using System.Linq;

class URI
{
    static void Main(string[] args)
    {
        List<Tuple<Func<decimal, decimal, bool>, string>> quadrantes =
            new List<Tuple<Func<decimal, decimal, bool>, string>>
            {
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => (x > 0 || x < 0) && y == 0, "Eixo X"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => (x == 0) && (y > 0 || y < 0), "Eixo Y"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => x > 0 && y > 0, "Q1"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => x < 0 && y > 0, "Q2"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => x < 0 && y < 0, "Q3"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => x > 0 && y < 0, "Q4"),
                new Tuple<Func<decimal, decimal, bool>, string>((x, y) => x == 0 && y == 0, "Origem")
            };

        string[] input = Console.ReadLine().Split();

        decimal X = Convert.ToDecimal(input[0]);

        decimal Y = Convert.ToDecimal(input[1]);

        var resultado = (quadrantes.First(f => f.Item1(X, Y)))?.Item2;

        Console.WriteLine(resultado);
    }
}