﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SequenciaNumerosESoma
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = Console.ReadLine().Split();
            var numeroUm = int.Parse(inputs[0]);
            var numeroDois = int.Parse(inputs[1]);
            var listaSequencia = new List<int[]>();

            while (numeroUm>0 && numeroDois>0)
            {
                listaSequencia.Add(numeroUm > numeroDois
                    ? Enumerable.Range(numeroDois, numeroUm-numeroDois+1).ToArray()
                    : Enumerable.Range(numeroUm, numeroDois-numeroUm+1).ToArray());

                inputs = Console.ReadLine().Split();
                numeroUm = int.Parse(inputs[0]);
                numeroDois = int.Parse(inputs[1]);
            }

            for (var i = 0; i < listaSequencia.Count; i++)
            {
                for (var j = 0; j < listaSequencia[i].Length; j++)
                {
                    Console.Write(listaSequencia[i][j]+" ");
                }
                Console.Write($"Sum={listaSequencia[i].Aggregate(((i1, i2) => i1+i2))}\n");
            }
        }
    }
}