﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParesEntre5Numeros
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = new List<int>();
            for (int i = 0; i < 5; i++)
                numeros.Add(Convert.ToInt32(Console.ReadLine()));        
            
            Console.WriteLine("{0} valores pares",numeros.Count(x=>x %2==0));
        }
    }
}