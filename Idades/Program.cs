﻿using System;

namespace Idades
{
    class Program
    {
        static void Main(string[] args)
        {
            var idades = 1;
            var contador = 0.0;
            var soma = 0.0;
            while ((idades = int.Parse(Console.ReadLine()))>0)
            {
                soma += idades;
                contador++;
            }
            Console.WriteLine("{0:0.00}",(soma/contador));
        }
    }
}