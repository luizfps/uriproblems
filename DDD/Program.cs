﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DDD
{
    class Program
    {
        static void Main(string[] args)
        {
            var ddds = new Dictionary<int, string>
            {
                {61, "Brasilia"},
                {71, "Salvador"},
                {11, "Sao Paulo"},
                {21, "Rio de Janeiro"},
                {32, "Juiz de Fora"},
                {19, "Campinas"},
                {27, "Vitoria"},
                {31, "Belo Horizonte"},
            };

            var ddd = Console.ReadLine().Trim().Split().Select(x=>Convert.ToInt32(x)).First();

            try
            {
                var destino = ddds[ddd];
                Console.WriteLine(ddds[ddd]);
            }
            catch (Exception e)
            {
                Console.WriteLine("DDD nao cadastrado");
            }
        }
    }
}