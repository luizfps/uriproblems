﻿using System;

namespace SequencialIJ4
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 1.2;
            double b = 2.2;
            double c = 3.2;
            Console.WriteLine($"I=0 J=1\nI=0 J=2\nI=0 J=3");
            for (double j = 0.2; j <= 2; j = j += 0.2)
            {
                if(Math.Round(j)%1==0)
                    Console.WriteLine($"I={j} J={a}\nI={j} J={b}\nI={j} J={c}");
                else
                    Console.WriteLine($"I={j} J={a:F1}\nI={j} J={b:F1}\nI={j} J={c:F1}");
                
                a += 0.2;
                b += 0.2;
                c += 0.2;
            }
        }
    }
}